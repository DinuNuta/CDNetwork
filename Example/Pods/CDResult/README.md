# CDResult

[![CI Status](http://img.shields.io/travis/DinuNuta/CDResult.svg?style=flat)](https://travis-ci.org/DinuNuta/CDResult)
[![Version](https://img.shields.io/cocoapods/v/CDResult.svg?style=flat)](http://cocoapods.org/pods/CDResult)
[![License](https://img.shields.io/cocoapods/l/CDResult.svg?style=flat)](http://cocoapods.org/pods/CDResult)
[![Platform](https://img.shields.io/cocoapods/p/CDResult.svg?style=flat)](http://cocoapods.org/pods/CDResult)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDResult is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CDResult"
```

## Author

DinuNuta, coscodan.dinu@gmail.com

## License

CDResult is available under the MIT license. See the LICENSE file for more info.
