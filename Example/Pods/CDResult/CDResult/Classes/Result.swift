import Foundation


public enum Result<T>
{
    case success(T)
    case error(Error)
}

public extension Result
{
    ///Functor
    public func map<U>(operation:(T)->U) -> Result<U>
    {
        switch self
        {
        case .success(let value):
            return .success(operation(value))
        case .error(let error):
            return .error(error)
        }
    }
    ///Monad
    public func flatMap<U>(operation:(T)->Result<U>) -> Result<U>
    {
        switch self
        {
        case .success(let value):
            return operation(value)
        case .error(let error):
            return .error(error)
        }
    }
    
    ///resolve result to catсh errors
    public func resolve() throws -> T
    {
        switch self
        {
        case .success(let value):
            return value
        case .error(let error):
            throw error
        }
    }
    
    ///init with throwing function
    public init(_ throwingExpression: () throws -> T )
    {
        do
        {
            let value = try throwingExpression()
            self = Result.success(value)
        }
        catch
        {
            self = Result.error(error)
        }
    }
}
