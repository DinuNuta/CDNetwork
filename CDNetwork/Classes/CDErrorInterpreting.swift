//
//  CDErrorInterpreting.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation

public protocol CDErrorInterpreting
{
    func error(with response: URLResponse?, data: Data?) -> Error?
}
