//
//  CDDataDownload+NotificationCenter.swift
//  CDNetwork
//
//  Created by Coscodan Dinu on 11/9/18.
//

import Foundation


public extension NotificationCenter {
    public func subscribeForLVManagerProgressChange(with block:@escaping (Download)->()) {
        addObserver(forName: .CDDownloadManagerProgress , object: nil, queue: .main) { notification in
            guard let download = notification.userInfo?["download"] as? Download else {return}
            block(download)
        }
    }
    
    public func subscribeForLVManagerStateChange(with block:@escaping (Download)->()) {
        addObserver(forName: .CDDownloadManagerState , object: nil, queue: .main) { notification in
            guard let download = notification.userInfo?["download"] as? Download else {return}
            block(download)
        }
    }
}


fileprivate enum DownloadUDKeys: String {
    case lvDownloads
}

protocol DownloadPersistantManaging {
    func sync(downloads:[Download])
    func getDownloads() -> [Download]
}

struct DownloadPersistantManager: DownloadPersistantManaging {
    func getDownloads() -> [Download] {
        guard let data = UserDefaults.standard.data(forKey: DownloadUDKeys.lvDownloads.rawValue),
            let downloads = try? PropertyListDecoder().decode([Download].self, from: data) else {return []}
        return downloads
    }
    
    func sync(downloads: [Download]) {
        let data = try? PropertyListEncoder().encode(downloads)
        UserDefaults.standard.set(data, forKey: DownloadUDKeys.lvDownloads.rawValue)
    }
}
