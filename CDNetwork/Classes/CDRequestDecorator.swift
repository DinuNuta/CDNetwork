//
//  CDRequestDecorator.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation


public protocol CDRequestDecorator: CDRequestType
{
    var target : CDRequestType {get}
}


public extension CDRequestDecorator
{
    public var method: HttpMethod{
        get { return target.method }
    }
    
    public var url: URL{
        get {return target.url }
    }
    
    public var request: URLRequest{
        get { return target.request }
    }
}
