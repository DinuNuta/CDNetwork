//
//  CDDataDownload.swift
//  CDNetwork
//
//  Created by Coscodan Dinu on 11/9/18.
//

import Foundation

public struct Download: Codable {
    public let url: URL
    public let id: String
    public let userInfo: [String:String]?
    
    public var progress: DownloadProgress = .zero
    public var state: DownloadState = .pending
    
    public var temporaryPath: URL?
    public var localPath: URL?
    var taskIdentifier: Int = 0
    var resumeData: Data?
    
    public init(url: URL, id: String, userInfo: [String:String]?) {
        self.url = url
        self.id = id
        self.userInfo = userInfo
    }
}

public enum DownloadState: Codable {
    case completed
    case pending
    case downloading
    case paused
    case canceled
    case failed(reason:String)
    
    private enum CodingKeys: String, CodingKey {
        case completed
        case pending
        case canceled
        case failed
        case paused
        case downloading
    }
    
    enum ProgressEncodingError: Error {
        case error(String)
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let _ = try? values.decode(Bool.self, forKey: .completed) {
            self = .completed
            return
        }
        if let _ = try? values.decode(Bool.self, forKey: .pending) {
            self = .pending
            return
        }
        if let _ = try? values.decode(Bool.self, forKey: .canceled) {
            self = .canceled
            return
        }
        if let reason = try? values.decode(String.self, forKey: .failed) {
            self = .failed(reason: reason)
            return
        }
        if let _ = try? values.decode(Bool.self, forKey: .downloading) {
            self = .downloading
            return
        }
        if let _ = try? values.decode(Bool.self, forKey: .paused) {
            self = .paused
            return
        }
        throw ProgressEncodingError.error("\(dump(values))")
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .downloading:
            try container.encode(true, forKey: .downloading)
        case .completed:
            try container.encode(true, forKey: .completed)
        case .canceled:
            try container.encode(true, forKey: .canceled)
        case .failed(reason: let reason):
            try container.encode(reason, forKey: .failed)
        case .pending:
            try container.encode(true, forKey: .pending)
        case .paused:
            try container.encode(true, forKey: .paused)
        }
    }
}

public struct DownloadProgress: Codable {
    public var progress:Float {
        return Float(recieved)/Float(expected)
    }
    
    public let recieved:Int64
    public let expected:Int64
    
    public static var zero: DownloadProgress = DownloadProgress(recieved: 0, expected: 1)
}
