//
//  CDDataUploader.swift
//  Pods
//
//  Created by Coscodan Dinu on 8/24/17.
//
//

import Foundation
import CDResult

public typealias DataUploadingProgressBlock = (UploadProgress) -> Void

public enum UploadProgress{
    case pending
    case uploading(progress:Double)
    case processingData
    case done
    case error
}


public protocol DataUploading : CDRequestSending{
     func uploadData(with request:CDRequestType, progressBlock: (DataUploadingProgressBlock)? , completion: ((Result<Data?>) -> ())? )
}

public extension DataUploading{
    public func send(request: CDRequestType, completion: ((Result<Data?>) -> ())?) {
        uploadData(with: request, progressBlock: nil, completion: completion)
    }
}

class TaskDelegate : NSObject, URLSessionDataDelegate {
    var progressClosure : DataUploadingProgressBlock?
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let progressPercentage = Double(totalBytesSent) / Double(totalBytesExpectedToSend)
        let progress = UploadProgress.uploading(progress: progressPercentage)
        print(progressPercentage)
        progressClosure?(progress)
    }
}

public struct CDDataUploader : DataUploading
{
    public var errorInterpreter: CDErrorInterpreting?
    private var session_delegate = TaskDelegate()
    
    public init(with errorInterpreter : CDErrorInterpreting)
    {
        self.errorInterpreter = errorInterpreter
    }
    
    public func uploadData(with request: CDRequestType, progressBlock: (DataUploadingProgressBlock)?, completion: ((Result<Data?>) -> ())?) {
        session_delegate.progressClosure = progressBlock
        let urlRequest = request.request
        
        let delegateQueue = OperationQueue()
        delegateQueue.qualityOfService = .userInitiated
        
        let session = URLSession(configuration: .default, delegate: session_delegate, delegateQueue: delegateQueue)
        let task = session.uploadTask(with: urlRequest, from: urlRequest.httpBody)
        { (data, response, error) in
            if let error = error {
                completion?(Result.error(error))
            }
            if let networkError = self.errorInterpreter?.error(with: response, data: data) {
                completion?(Result.error(networkError))
            }
            else {
                completion?(Result.success(data))
            }
        }
        task.resume()
    }
}
