# CDNetwork

[![CI Status](http://img.shields.io/travis/DinuNuta/CDNetwork.svg?style=flat)](https://travis-ci.org/DinuNuta/CDNetwork)
[![Version](https://img.shields.io/cocoapods/v/CDNetwork.svg?style=flat)](http://cocoapods.org/pods/CDNetwork)
[![License](https://img.shields.io/cocoapods/l/CDNetwork.svg?style=flat)](http://cocoapods.org/pods/CDNetwork)
[![Platform](https://img.shields.io/cocoapods/p/CDNetwork.svg?style=flat)](http://cocoapods.org/pods/CDNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDNetwork is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CDNetwork"
```

## Author

DinuNuta, coscodan.dinu@gmail.com

## License

CDNetwork is available under the MIT license. See the LICENSE file for more info.
