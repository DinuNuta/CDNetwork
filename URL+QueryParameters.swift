//
//  URL+QueryParameters.swift
//  CDNetwork
//
//  Created by Coscodan Dinu on 11/9/18.
//

import Foundation

public extension URL{
    public func appendingQuery(withParameters parameters:[URLQueryItem]) -> URL{
        
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters
        }
        
        return components.url!
    }
}
